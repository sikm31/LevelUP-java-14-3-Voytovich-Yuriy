<%--
  Created by IntelliJ IDEA.
  User: y.voytovich
  Date: 17.06.2015
  Time: 17:04
  To change users template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="application/javascript" src="resources/js/main.js"></script>
</head>
<body><table border="2px">
    <tr>
        <td>Выбор</td>
        <td>Удаление</td>
        <td>users.id</td>
        <td>users.display_name</td>
        <td>users.user_login</td>
        <td>users.user_pass</td>
        <td>users.user_nicename</td>
        <td>users.user_email</td>
        <td>users.user_url</td>
        <td>users.user_registered</td>
        <td>users.user_activation</td>
        <td>users.user_status</td>
        <td>users.display_name</td>
    </tr>
<c:forEach items="${users}" var="users">
    <tr>
        <td ><a href="getid/${users.id}">Выбор</a></td>
        <td >${user.user_login}</td>
        <td><a href="delete/${users.id}">Удалить</a></td>
        <td><c:out value="${users.id}" /></td>
        <td> <c:out value="${users.display_name}" /></td>
        <td> <c:out value="${users.user_login}" /></td>
        <td> <c:out value="${users.user_pass}" /></td>
        <td> <c:out value="${users.user_nicename}" /></td>
        <td> <c:out value="${users.user_email}" /></td>
        <td> <c:out value="${users.user_url}" /></td>
        <td> <c:out value="${users.user_registered}" /></td>
        <td> <c:out value="${users.user_activation_key}" /></td>
        <td> <c:out value="${users.user_status}" /></td>
        <td> <c:out value="${users.display_name }" /></td>
    </tr>

</c:forEach>
</table>
<form:form method="post" action="add" commandName="user">

    <table>
        <tr>
            <td><form:label path="user_login">Login</form:label></td>
            <td><form:input path="user_login" /></td>
        </tr>
        <tr>
            <td><form:label path="user_pass">Password</form:label></td>
            <td><form:input path="user_pass" /></td>
        </tr>
        <tr>
            <td><form:label path="user_email">E-mail</form:label></td>
            <td><form:input path="user_email" /></td>
        </tr>
        <tr>
            <td><form:label path="display_name">Display_name</form:label></td>
            <td><form:input path="display_name" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit"
                                   value="adduser" /></td>
        </tr>
    </table>
</form:form>





</body>
</html>
