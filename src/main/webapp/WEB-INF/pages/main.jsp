<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Онлайн библиотека</title>
    <link rel="stylesheet" href="resources/css/main.css" type="text/css"/>
    <script type="application/javascript" src="resources/js/jquery.js"></script>
    <script type="application/javascript" src="resources/js/main.js"></script>
</head>
<body>

<div class="container">

    <div class="header">
        <div class="logo">
            <img src="resources/images/logo.jpg" alt="Логотип" user_login="logo" />

        </div>
        <div class="descr">
            Онлайн библиотека
        </div>
        <div class="search_form">
            <form user_login="search_form" method="POST">
                <input type="text" user_login="search_String" value="" size="110" />
                <input class="search_button" type="submit" value="Поиск" user_login="search_button" />
                <select user_login="search_option">
                    <option>Название</option>
                    <option>Автор</option>
                </select>
            </form>
        </div>
    </div>
<c:forEach items="${menu}" var="menu">
  <tr>
    <td>${menu.id}</td>
    </tr>
    <tr>
    <td>${menu.name}</td>
    </tr>
    <tr>
        <td>${menu.slug}</td>
    </tr>
</c:forEach>
    <hr>
<c:forEach items="${post}" var="post">
    <tr>
        <td>${post.id}</td>
    </tr>
    <tr>
        <td>${post.name}</td>
    </tr>
    <tr>
        <td>${post.content}</td>
    </tr>
</c:forEach>
</body>
</html>
