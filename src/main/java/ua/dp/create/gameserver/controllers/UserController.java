package ua.dp.create.gameserver.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ua.dp.create.gameserver.domains.User;
import ua.dp.create.gameserver.services.UserService;

import javax.annotation.Resource;
import java.sql.SQLException;

/**
 * Created by y.voytovich on 17.06.2015.
 */
@Controller

public class UserController {

    @Autowired
    UserService userService;

    protected static Logger logger = Logger.getLogger("controller-user");

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String listUsers(Model model) throws SQLException {
        logger.debug("Received request to show all users");
        model.addAttribute("user", new User());
        model.addAttribute("users", userService.getAllUsers());

        return "userspage";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user,BindingResult result,Model model) throws SQLException {
        userService.addUser(user);

        return "redirect:/users";
    }


    @RequestMapping("/delete/{userId}")
    public String deleteUser(@PathVariable("userId") long userId) throws SQLException {
        deleteUser(userId);

        return "redirect:/users";
    }

    @RequestMapping(value = "/getid/{userid}", method = RequestMethod.GET,
            produces = "application/json", headers = "Accept=application/json")
    @ResponseBody
    public User getUserById(@PathVariable("userid") long userid) throws SQLException {
        User userById = userService.getUserById(userid);
       return userById;

    }

    @RequestMapping(value = "/putid", method = RequestMethod.GET, consumes = "application/json")
    public ResponseEntity<String> getUserById(@RequestBody User userById){
        logger.info(userById.getDisplay_name());
        return new ResponseEntity<String>(HttpStatus.ACCEPTED);
    }






}
