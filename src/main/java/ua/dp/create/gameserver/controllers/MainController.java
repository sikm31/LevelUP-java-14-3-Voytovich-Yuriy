package ua.dp.create.gameserver.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.create.gameserver.domains.Menu;
import ua.dp.create.gameserver.domains.Post;
import ua.dp.create.gameserver.repositories.MenuListDaoImpl;
import ua.dp.create.gameserver.repositories.PostListDaoShort;
import ua.dp.create.gameserver.services.MenuListService;
import ua.dp.create.gameserver.services.PostListService;
import ua.dp.create.gameserver.services.PostListServiceShort;

import java.sql.SQLException;

/**
 * Created by y.voytovich on 08.06.2015.
 */
@Controller
@RequestMapping("/")
public class MainController {

   @Autowired
   private MenuListService menulistservice;

   @Autowired
   private PostListService postListService;

    @Autowired
    private PostListServiceShort postListServiceShort;


//    @RequestMapping(method = RequestMethod.GET)
//    public ModelAndView main() {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("main");
//        modelAndView.addObject("post", postListService.getAllPosts());
//        modelAndView.addObject("menu", menulistservice.getMenuList());
//        return modelAndView;//new ModelAndView("main", "menu", menulistservice.getMenuList());
//    }

    @RequestMapping(method = RequestMethod.GET)
    public String main(Model model) {
        model.addAttribute("menu", menulistservice.getMenuList());
        model.addAttribute("post", postListServiceShort.getAllPostsShort());
        return "main";
    }

    @RequestMapping(value = "/post",method = RequestMethod.GET)
    public ModelAndView main2() throws SQLException {
        return new ModelAndView("main", "post", postListService.getPostByMenu(708));
    }



}
