package ua.dp.create.gameserver.services;

import org.springframework.stereotype.Service;
import ua.dp.create.gameserver.domains.Post;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 */

public interface PostListService {

    List<Post> getAllPosts();
    List<Post> getPostByMenu(long id) throws SQLException;

}
