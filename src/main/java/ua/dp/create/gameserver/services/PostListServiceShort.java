package ua.dp.create.gameserver.services;

import ua.dp.create.gameserver.domains.Post;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 */

public interface PostListServiceShort {

    List<Post> getAllPostsShort();
    List<Post> getPostShortByMenu(long id) throws SQLException;

}
