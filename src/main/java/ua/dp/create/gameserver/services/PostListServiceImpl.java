package ua.dp.create.gameserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.create.gameserver.domains.Post;
import ua.dp.create.gameserver.repositories.PostListDao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 */
@Service
public class PostListServiceImpl implements PostListService {

    @Autowired
    private PostListDao postListDao;

    @Override
    public List<Post> getAllPosts() {
            return postListDao.getPosts();

    }

    @Override
    public List<Post> getPostByMenu(long id) throws SQLException {
        return postListDao.getPostByMenu(id);
    }
}
