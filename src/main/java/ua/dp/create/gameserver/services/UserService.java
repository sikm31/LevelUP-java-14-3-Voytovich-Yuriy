package ua.dp.create.gameserver.services;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.dp.create.gameserver.domains.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 17.06.2015.
 */
public interface UserService {
    public List<User> getAllUsers() throws SQLException;
    public void addUser(User user) throws SQLException;
    public void updateUser(User user) throws SQLException;
    public User getUserById(Long id) throws SQLException;
    public void deleteUser(long id) throws SQLException;
    public void getEditUsers(long id) throws SQLException;



}
