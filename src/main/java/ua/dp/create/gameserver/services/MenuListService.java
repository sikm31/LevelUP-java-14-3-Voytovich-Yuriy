package ua.dp.create.gameserver.services;

import org.springframework.stereotype.Service;
import ua.dp.create.gameserver.domains.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 */
public interface MenuListService {
    List<Menu> getMenuList();

}
