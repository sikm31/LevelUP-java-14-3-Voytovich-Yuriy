package ua.dp.create.gameserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.create.gameserver.domains.Post;
import ua.dp.create.gameserver.repositories.PostListDaoShort;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 09.06.2015.
 */
@Service
public class PostListServiceShortImpl implements PostListServiceShort {

    @Autowired
    private PostListDaoShort postListDaoShort;

    @Override
    public List<Post> getAllPostsShort() {
        return postListDaoShort.getPosts();
    }

    @Override
    public List<Post> getPostShortByMenu(long id) throws SQLException {
        return postListDaoShort.getPostShortByMenu(id);
    }
}
