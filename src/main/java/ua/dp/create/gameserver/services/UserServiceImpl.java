package ua.dp.create.gameserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.create.gameserver.domains.User;
import ua.dp.create.gameserver.repositories.UserDao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 17.06.2015.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public List<User> getAllUsers() throws SQLException {
        return userDao.getAllUsers();
    }

    @Override
    public void addUser(User user) throws SQLException {
        userDao.addUser(user);
    }

    @Override
    public void updateUser(User user) throws SQLException {

    }

    @Override
    public User getUserById(Long id) throws SQLException {
      return userDao.getUserById(id);
    }

    @Override
    public void deleteUser(long id) throws SQLException {
        userDao.deleteUser(id);
    }

    @Override
    public void getEditUsers(long id) throws SQLException {
        userDao.getEditUsers(id);
    }


}
