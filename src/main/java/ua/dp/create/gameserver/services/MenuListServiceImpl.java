package ua.dp.create.gameserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.create.gameserver.domains.Menu;
import ua.dp.create.gameserver.repositories.MenuListDao;
import ua.dp.create.gameserver.repositories.MenuListDaoImpl;
import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 */
@Service
public class MenuListServiceImpl implements MenuListService {

    @Autowired
    public MenuListDao menuListDao;


    @Override
    public List<Menu> getMenuList() {
            return menuListDao.getMenues();

    }

}
