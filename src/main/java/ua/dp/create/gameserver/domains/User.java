package ua.dp.create.gameserver.domains;



import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by y.voytovich on 09.06.2015.
 */
@Entity
@Table(name = "wp_users")
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    private long id;

    @Size(min = 5, message = "Имя должно быть больше 5 символов")
    @Column(name = "user_login")
    private String user_login;

    @Size(min = 5, max = 10, message = "Пароль должен быть от 5 до 10 символов")
    @Column(name = "user_pass")
    private  String user_pass;

    @Column(name = "user_nicename")
    private String user_nicename;

    @Column(name = "user_email")
    private String user_email;

    @Column(name = "user_url")
    private String user_url;

    @Column(name = "user_registered")
    private Date user_registered;

    @Column(name = "user_activation_key")
    private String user_activation_key;

    @Column(name = "user_status")
    private int user_status;

    @Column(name = "display_name")
    private String display_name;

    public User() {
    }

    public User(long id, String user_login, String user_pass, String user_nicename, String user_email, String user_url, Date user_registered, String user_activation_key, int user_status, String display_name) {
        this.id = id;
        this.user_login = user_login;
        this.user_pass = user_pass;
        this.user_nicename = user_nicename;
        this.user_email = user_email;
        this.user_url = user_url;
        this.user_registered = user_registered;
        this.user_activation_key = user_activation_key;
        this.user_status = user_status;
        this.display_name = display_name;
    }

    public String getUser_nicename() {
        return user_nicename;
    }

    public void setUser_nicename(String user_nicename) {
        this.user_nicename = user_nicename;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_url() {
        return user_url;
    }

    public void setUser_url(String user_url) {
        this.user_url = user_url;
    }

    public Date getUser_registered() {
        return user_registered;
    }

    public void setUser_registered(Date user_registered) {
        this.user_registered = user_registered;
    }

    public String getUser_activation_key() {
        return user_activation_key;
    }

    public void setUser_activation_key(String user_activation_key) {
        this.user_activation_key = user_activation_key;
    }

    public int getUser_status() {
        return user_status;
    }

    public void setUser_status(int user_status) {
        this.user_status = user_status;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public void setUser_pass(String user_pass) {
        this.user_pass = user_pass;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }
}
