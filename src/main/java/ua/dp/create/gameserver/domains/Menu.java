package ua.dp.create.gameserver.domains;

/**
 * Created by y.voytovich on 05.06.2015.
 *
 * Create domains Menu
 */
public class Menu {

    private String name;
    private long id;
    private String slug;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
