package ua.dp.create.gameserver.domains;

/**
 Create domains Authors
 */
public class Author {

    private String name;
    private long id;

    public Author() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
