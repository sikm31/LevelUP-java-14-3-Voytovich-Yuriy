package ua.dp.create.gameserver.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.dp.create.gameserver.domains.Menu;


import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 * Created by y.voytovich on 05.06.2015.
 *
 * Implements Dao. Recieve from Database Menu
 */
@Component
public class MenuListDaoImpl implements MenuListDao {

    @Autowired // recieve bean datasource look mvc-dispatcher-servlet.xml
    private DataSource dataSource;



    @Override
    public List<Menu> getMenues() { // Recieve menu from Database

        List<Menu> menuListDao = new ArrayList<Menu>();

        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        Statement stmt2 = null;
        ResultSet rs2 = null;
        Connection conn2 = null;
        try {
            conn = dataSource.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT terms.term_id,terms.name,terms.slug,taxonomy.description FROM loveukr.wp_terms as terms inner join loveukr.wp_term_taxonomy as taxonomy on terms.term_id=taxonomy.term_id where taxonomy.taxonomy='category' and taxonomy.parent=0 order by name");
            conn2 = dataSource.getConnection();
            stmt2 = conn.createStatement();
            rs2 = stmt2.executeQuery("SELECT terms.term_id,terms.name,terms.slug,taxonomy.description FROM loveukr.wp_terms as terms inner join loveukr.wp_term_taxonomy as taxonomy on terms.term_id=taxonomy.term_id where taxonomy.taxonomy='category' and taxonomy.parent<>0 order by name");
            while (rs.next()){
                Menu menu = new Menu();
                menu.setName(rs.getString("name"));
                menu.setId(rs.getLong("term_id"));
                menu.setSlug(rs.getString("slug"));
                menuListDao.add(menu);
                while (rs2.next()){
                    Menu menu2 = new Menu();
                    menu2.setName(rs2.getString("name"));
                    menu2.setId(rs2.getLong("term_id"));
                    menu2.setSlug(rs2.getString("slug"));
                    menuListDao.add(menu2);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(MenuListDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally { // If connection not NULL close
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(MenuListDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return menuListDao;

    }



}
