package ua.dp.create.gameserver.repositories;

import ua.dp.create.gameserver.domains.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 *
 *
 * Interface for Dao Menu
 */
public interface MenuListDao {

    List<Menu> getMenues();
}
