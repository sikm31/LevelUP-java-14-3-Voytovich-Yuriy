package ua.dp.create.gameserver.repositories;

import org.springframework.context.annotation.Bean;
import ua.dp.create.gameserver.domains.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 16.06.2015.
 */
public interface UserDao {

    public void addUser(User user) throws SQLException;
    public void updateUser(User user) throws SQLException;
    public User getUserById(Long id) throws SQLException;
    public List<User> getAllUsers() throws SQLException;
    public void deleteUser(long id) throws SQLException;
    public void getEditUsers(long id) throws SQLException;

}
