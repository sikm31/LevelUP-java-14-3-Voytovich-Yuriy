package ua.dp.create.gameserver.repositories;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.create.gameserver.domains.User;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by y.voytovich on 17.06.2015.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    protected static Logger logger = Logger.getLogger("UserDao");

    @Autowired
    private SessionFactory sessionFactory;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addUser(User user) throws SQLException {
        Calendar Current_Calendar = Calendar.getInstance();
        Date Current_Date = Current_Calendar.getTime();
        user.setUser_registered(Current_Date);
        user.setUser_activation_key("0");
        user.setUser_status(0);
        user.setUser_nicename(user.getDisplay_name());
        user.setUser_url(user.getUser_email());
        sessionFactory.getCurrentSession().save(user);


    }

    @Override
    public void updateUser(User user) throws SQLException {

    }

    @Override
    public User getUserById(Long id) throws SQLException {
        return entityManager.find(User.class, id);


    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        logger.debug("Retrieving all users");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM  User");

        return query.list();
    }

    @Override
    public void deleteUser(long id) throws SQLException {
        User user = (User) sessionFactory.getCurrentSession().load(User.class, id);
        if (null != user) sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public void getEditUsers(long id) throws SQLException {
        User user = (User) sessionFactory.getCurrentSession().load(User.class, id);

    }
}
