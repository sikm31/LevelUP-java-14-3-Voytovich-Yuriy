package ua.dp.create.gameserver.repositories;


import ua.dp.create.gameserver.domains.Post;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 *
 * Interface Dao Post
 */
public interface PostListDao {

    List<Post> getPosts();
    List<Post> getPostByMenu(long id) throws SQLException;
}
