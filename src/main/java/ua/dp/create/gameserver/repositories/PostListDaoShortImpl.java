package ua.dp.create.gameserver.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.dp.create.gameserver.domains.Post;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by y.voytovich on 09.06.2015.
 *
 * Implements short post from Databases
 */
@Component
public class PostListDaoShortImpl implements PostListDaoShort {
    @Autowired
    private DataSource dataSource;


    @Override
    public List<Post> getPosts() {

        String sql = "SELECT terms.term_id,terms.name,terms.slug,taxonomy.taxonomy,taxonomy.description,relation.object_id,posts.post_content,posts.post_title,posts.post_status,posts.post_date,posts.post_name " +
                "FROM loveukr.wp_terms as terms " +
                "inner join loveukr.wp_term_taxonomy as taxonomy on terms.term_id=taxonomy.term_id " +
                "inner join loveukr.wp_term_relationships as relation on taxonomy.term_taxonomy_id = relation.term_taxonomy_id " +
                "inner join loveukr.wp_posts as posts on relation.object_id = posts.ID group by relation.object_id having count(object_id)>0";
        List<Post> postListDaoShort = new ArrayList<Post>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = dataSource.getConnection();

            stmt = conn.createStatement();
            System.out.println(sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Post post = new Post();
                post.setId(rs.getLong("terms.term_id"));
                post.setName(rs.getString("terms.name"));
                post.setTitle(rs.getString("post_title"));
                try {
                    String s = rs.getString("post_content");
                    if (s.length() > 400) {
                        post.setContent(s.substring(0, 400));
                    } else post.setContent(s.substring(0, s.length()));
                } catch (StringIndexOutOfBoundsException e) {
                    // post.setContent(rs.getString("post_content"));
                    System.out.println("Error");
                }
                post.setPostDate(rs.getDate("posts.post_date"));
                post.setObjectId(rs.getInt("relation.object_id"));
                post.setSlug(rs.getString("terms.slug"));
                postListDaoShort.add(post);
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostListDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PostListDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return postListDaoShort;
    }

    @Override
    public List<Post> getPostShortByMenu(long id){

        String sql = "SELECT terms.term_id,terms.name,terms.slug,taxonomy.taxonomy,taxonomy.description,relation.object_id,posts.post_content,posts.post_title,posts.post_status,posts.post_date,posts.post_type,posts.post_name " +
                "FROM loveukr.wp_terms as terms " +
                "inner join loveukr.wp_term_taxonomy as taxonomy on terms.term_id=taxonomy.term_id " +
                "inner join loveukr.wp_term_relationships as relation on taxonomy.term_taxonomy_id = relation.term_taxonomy_id " +
                "inner join loveukr.wp_posts as posts on relation.object_id = posts.ID " +
                "where posts.post_type='post' and relation.object_id=? group by relation.object_id having count(object_id)>0";
        List<Post> postListDaoShortByMenu = new ArrayList<Post>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try{
        conn = dataSource.getConnection();
        stmt = conn.prepareStatement(sql);
        stmt.setLong(1, id);
        rs = stmt.executeQuery();
        while (rs.next()) {
            Post post = new Post();
            post.setId(rs.getLong("terms.term_id"));
            post.setName(rs.getString("terms.name"));
            post.setTitle(rs.getString("post_title"));
            try {
                String s = rs.getString("post_content");
                if (s.length() > 400) {
                    post.setContent(s.substring(0, 400));
                } else post.setContent(s.substring(0, s.length()));
            } catch (StringIndexOutOfBoundsException e) {
                // post.setContent(rs.getString("post_content"));
                System.out.println("Error");
            }
            post.setPostDate(rs.getDate("posts.post_date"));
            post.setObjectId(rs.getInt("relation.object_id"));
            post.setSlug(rs.getString("terms.slug"));
            postListDaoShortByMenu.add(post);
        }
        }catch (SQLException ex){
            Logger.getLogger(PostListDaoShortImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally { // If connection not NULL close
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PostListDaoShortImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return postListDaoShortByMenu;
    }
}