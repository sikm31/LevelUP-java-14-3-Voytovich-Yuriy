package ua.dp.create.gameserver.repositories;


import ua.dp.create.gameserver.domains.Post;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by y.voytovich on 05.06.2015.
 *
 * Dao interface for short post
 */
public interface PostListDaoShort {

    List<Post> getPosts();
    List<Post> getPostShortByMenu(long id) throws SQLException;
}
